import { Injectable } from '@angular/core';
import { $WebSocket } from 'angular2-websocket/angular2-websocket';
import { appConfig } from '../constants/app.config';

import { Observable } from 'rxjs/Observable';
import { ResponseModel } from '../model/response.model';


@Injectable()
export class SocketService {
  public ws: any;

  constructor() {
    this.ws = new $WebSocket(appConfig.apiUrl);
    this.sendReq();
  }

  sendReq(req?) {
    this.ws.send(req ? req : appConfig.res).subscribe(
      (msg: ResponseModel) => {
        this.ws.close(false);
      },
      (msg: ResponseModel) => {},
      () => {}
    );
  }

  getData(): Observable<any> {
    return this.ws.getDataStream();
  }
}
