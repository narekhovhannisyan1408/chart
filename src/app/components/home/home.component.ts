import { Component, OnInit, ViewChild } from '@angular/core';
import { appConfig } from '../../constants/app.config';
import { SocketService } from '../../services/socket.service';
import { DynoGraphModel, ResponseModel } from '../../model/response.model';
import { BaseChartDirective } from 'ng2-charts/ng2-charts';
import { FormBuilder, FormGroup } from '@angular/forms';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  public invalidId: boolean;
  public data: DynoGraphModel;
  public lineChartData = [];
  public lineChartOptions = appConfig.lineChartOptions;
  public lineChartLegend = appConfig.lineChartLegend;
  public lineChartType = appConfig.lineChartType;
  public lineChartColors = [];
  private maxMinPoints: any = {};

  IdForm: FormGroup;

  @ViewChild('myChart')
  myChart: BaseChartDirective;

  constructor(private svc: SocketService,
              private fb: FormBuilder) {
  }

  ngOnInit() {
    this.svc.getData().subscribe((res: ResponseModel) => {
      this.lineChartData = [];
      if (JSON.parse(res['data']).body) {
        this.invalidId = false;
        this.data = JSON.parse(res['data']).body.report.dynoGraph;
        const downholeCardMaxMinXY = {
          maxX: this.data.downholeCard.maxPosition,
          minX: this.data.downholeCard.minPosition,
          maxY: this.data.downholeCard.maxLoad,
          minY: this.data.downholeCard.minLoad,
        };
        const surfaceCardMaxMinXY = {
          maxX: this.data.surfaceCard.maxPosition,
          minX: this.data.surfaceCard.minPosition,
          maxY: this.data.surfaceCard.maxLoad,
          minY: this.data.surfaceCard.minLoad,
        };
        this.getPositionInterval(downholeCardMaxMinXY, surfaceCardMaxMinXY);
        for (const key of Object.keys(this.data)) {
          this.generateObject(key);
        }
      } else {
        this.invalidId = true;
      }
    });
    this.createForm();
  }

  createForm() {
    this.IdForm = this.fb.group({
      name: [''],
    });
  }

  generateObject(key) {
    const array = this.generateCords(key);
    array.push({
      x: array[0] ? array[0].x : null,
      y: array[0] ? array[0].y : null
    });
    const leftEnd = Math.ceil(array.length / 2);

    const result = {
      left: array.slice(0, leftEnd),
      right: array.slice(leftEnd)
    };

    const color = `rgba(${Math.floor((Math.random() * 130) +
      100)}, ${Math.floor((Math.random() * 130) + 100)}, ${Math.floor((Math.random() * 130) + 100)}`;
    this.lineChartData.push(
      {
        data: result.left,
        borderColor: color + ',1)',
        borderDash: [5, 5],
        borderWidth: 3,
        fill: false
      },
      {
        data: array,
        backgroundColor: color + ',0.5)',
        borderColor: 'transparent',
        borderWidth: 0
      },
      {
        data: result.right,
        borderColor: color + ',1)',
        borderWidth: 3,
        fill: false
      });
    this.lineChartColors.push(...appConfig.lineChartColors);
  }

  generateCords(key) {
    const array = [];
    if (key === 'permissibleLoads') {
      for (let i = 0; i < this.data[key].load.length; i++) {
        if (this.data[key].position[i] >= this.maxMinPoints.minX &&
          this.data[key].position[i] <= this.maxMinPoints.maxX &&
          this.data[key].load[i] >= this.maxMinPoints.minY &&
          this.data[key].load[i] <= this.maxMinPoints.maxY) {
          array.push({
            x: this.data[key].position[i],
            y: this.data[key].load[i]
          });
        }
      }
    } else {
      for (let i = 0; i < this.data[key].load.length; i++) {
        const item = this.data[key];
        array.push({
          x: item.position[i] ? item.position[i].toFixed(2) : null,
          y: item.load[i] ? item.load[i].toFixed(2) : null
        });
      }
    }
    return array;
  }

  getPositionInterval(downholeCard, surfaceCard) {
    this.maxMinPoints.maxX = Math.max(downholeCard.maxX, surfaceCard.maxX);
    this.maxMinPoints.minX = Math.min(downholeCard.minX, surfaceCard.minX);
    this.maxMinPoints.maxY = Math.max(downholeCard.maxY, surfaceCard.maxY);
    this.maxMinPoints.minY = Math.min(downholeCard.minY, surfaceCard.minY);
  }

  addItem() {
    const newReq = appConfig.res;
    newReq.body.id = this.IdForm.value.name;
    this.svc.sendReq(newReq);
  }
}
