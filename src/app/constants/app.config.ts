export const appConfig = {
  apiUrl: 'ws://13.72.109.59:3000',
  res: {
    'header': {'priority': 1, 'name': 'SimulateWell::REQ'},
    'body': {
      'id': '5b87b09e94412c12e263f734'
    }
  },
  lineChartColors: [{
    pointBackgroundColor: 'rgba(0, 0, 0, 0)',
    pointBorderColor: 'rgba(0, 0, 0, 0)'
  }, {
    pointBackgroundColor: 'rgba(0, 0, 0, 0)',
    pointBorderColor: 'rgba(0, 0, 0, 0)'
  }, {
    pointBackgroundColor: 'rgba(0, 0, 0, 0)',
    pointBorderColor: 'rgba(0, 0, 0, 0)'
  }],
  lineChartOptions: {
    responsive: true,
    pan: {
      enabled: true,
      mode: 'xy'
    },
    zoom: {
      enabled: true,
      mode: 'xy',
      limits: {
        max: 10,
        min: 0.5
      }
    },
    tooltips: {
      titleFontStyle: 'normal',
      custom: function (tooltip) {
        if (!tooltip) {
          return;
        } else {
          tooltip.displayColors = false;
        }
      },
      callbacks: {
        label: function (tooltipItem) {
          return 'X ' + tooltipItem.xLabel + ' : ' +  'Y ' + tooltipItem.yLabel;
        },
        title: function () {
          return;
        }
      }
    },
    elements: {
      line: {
        fill: true
      }
    },
    scales: {
      xAxes: [{
        display: true,
        type: 'linear',
        ticks: {
          userCallback: label => {
            if (Math.floor(label) === label) {
              return label;
            }
          },
        }
      }],
      yAxes: [{
        ticks: {
          userCallback: label => {
            if (Math.floor(label) === label) {
              return label;
            }
          },
        }
      }],
    }
  },
  lineChartLegend: false,
  lineChartType: 'line',
  customCords: [{
    x: -80,
    y: 15000
  }, {
    x: -60,
    y: 20000
  }, {
    x: 20,
    y: 15000
  }, {
    x: -60,
    y: 10000
  }]
};
