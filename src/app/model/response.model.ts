export class ResponseModel {
  constructor(public report: ReportModel) {
  }
}

export class ReportModel {
  constructor(public axLoads: AxLoadsModel,
              public dynoGraph: DynoGraphModel) {
  }
}

export class AxLoadsModel {
  constructor(public bucklingTendency: Array<number>,
              public depth: Array<number>,
              public effectiveMaxLoad: Array<number>,
              public effectiveMinLoad: Array<number>,
              public rodIndex: Array<number>,
              public rodsCount: number,
              public trueMaxLoad: Array<number>,
              public trueMinLoad: Array<number>) {
  }
}

export class DynoGraphModel {
  constructor(public downholeCard: CardModel,
              public permissibleLoads: PositionModel,
              public surfaceCard: CardModel) {
  }
}


export class PositionModel {
  constructor(public load: Array<number>,
              public position: Array<number>) {
  }
}

export class CardModel {
  constructor(public fluidLoad: number,
              public grossStroke: number,
              public load: Array<number>,
              public maxLoad: number,
              public maxPosition: number,
              public minLoad: number,
              public minPosition: number,
              public netStroke: number,
              public position: Array<number>,
              public weightedMaxLoad: number,
              public weightedMinLoad: number) {
  }
}
